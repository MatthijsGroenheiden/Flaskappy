FROM alphine
RUN apk add --no-cache python3 -dev \
    && pip3 install --upgrade pip

WORKDIR /Event_app
COPY . /Event_app

RUN pip3 install flask
RUN pip3 install sqlalchemy
RUN pip3 install sklearn
RUN pip3 install flask_wtf
RUN pip3 install wtforms

EXPOSE 8080

ENTRYPOINT ["python3"]
CMD ["flaskapp.py"]