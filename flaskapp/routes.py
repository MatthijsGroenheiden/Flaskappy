from flask import render_template, redirect, url_for, flash, request
from flaskapp.models import User_table, Events_table, User_event_history_table 
from flaskapp.forms import RegistrationForm, LoginForm, EventsRegistrationForm
from flaskapp import app, db, bcrypt
from flask_login import login_user, current_user, logout_user, login_required

@app.route("/")
@app.route("/home")
def home():
    
    return render_template(
        "home.html",
        
    )

@app.route("/register", methods=["GET", "POST"])
def register():
    #als user al is ingelogt return naar home
    if current_user.is_authenticated:
        return redirect(url_for("home"))
    # load forms
    form = RegistrationForm()
    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user1 = User_table(username=form.username.data, firstname=form.firstname.data, lastname=form.lastname.data, email=form.email.data, password=hashed_password)
        db.session.add(user1)
        db.session.commit()
        flash("account created for %s!"% (form.username.data), "succes" )
        return redirect(url_for("login"))

    return render_template(
        "register.html",
        title='Register',
        form=form
    )

@app.route("/login", methods=["GET", "POST"])
def login():
    #als user al is ingelogt return naar home
    if current_user.is_authenticated:
        return redirect(url_for("home"))
        # load forms
    form = LoginForm()
    # als submit klopt. filter op email naar user in database en decrypt ww in db en vergelijk met getypte ww. als dat klopt log user in en neem remember data mee
    if form.validate_on_submit():
        user = User_table.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('login failed, pleas check email and password', 'danger')
            return redirect(url_for("login"))
   
    return render_template(
        "login.html",
        title='Login',
        form=form
    )

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))
    
@app.route("/account")
@login_required
def account():
    return render_template('account.html', title='Account')

@app.route("/addevents", methods=["GET", "POST"])
@login_required
def addevents():
    form1 = EventsRegistrationForm()
    if form1.validate_on_submit():
        event = Events_table(eventname=form1.eventname.data, tag=form1.tag.data, discription=form1.discription.data, venue=form1.venue.data,eventstartingdate=form1.eventstartingdate.data,eventendingdate=form1.eventendingdate.data)
        db.session.add(event)
        db.session.commit()
        return redirect(url_for("addevents"))

    return render_template(
        "addevent.html",
        title="Create event",
        form=form1
    )

@app.route("/Api_info", methods=["GET"])
@login_required
def api_info():
    return render_template(
        "api_info.html"
    )

@app.route("/events", methods=["GET", "POST"])
@login_required
def events():
    events = Events_table.query.all()
    return render_template(
        "events.html",
        events=events
    )