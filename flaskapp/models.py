from datetime import datetime
from flaskapp import db, login_manager, bcrypt, development_mode
from flask_login import UserMixin


@login_manager.user_loader
def load_user(user_id):
    return User_table.query.get(int(user_id))

class User_table(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80),unique=True, nullable=False)
    firstname = db.Column(db.String(80), nullable=False)
    lastname = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(80),unique=True, nullable=False)
    password = db.Column(db.String(80), nullable=False)
    image_file = db.Column(db.String(20), nullable=True, default='default.jpg')
    user_event_history = db.relationship("User_event_history_table", backref="event_history")
    
    def __repr__(self):
        return f"User('{self.username}','{self.email}')"



class User_event_history_table(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    user = db.Column(db.Integer, db.ForeignKey('user_table.id'),
        nullable=False)
    event_name =  db.Column(db.String(80), nullable=False)
    event_discription = db.Column(db.String(80), nullable=False)
    event_date = db.Column(db.DateTime, nullable=False, default=2000-1-1)
    
    def __repr__(self):
        return f"User('{self.date}', '{self.user}','{self.event_name}')"
        


class Events_table(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    eventname =  db.Column(db.String(80), nullable=False)
    tag =  db.Column(db.String(80), nullable=False)
    discription = db.Column(db.String(80), nullable=False)
    venue =  db.Column(db.String(80), nullable=False)
    eventstartingdate = db.Column(db.DateTime, nullable=False)
    eventendingdate = db.Column(db.DateTime, nullable=False)

    def __repr__(self):
        return f"Events('{self.eventname}', '{self.tag}','{self.discription}')"

if development_mode == True:
    db.drop_all()
    db.create_all()
else:
    pass

class test_input():
    hashed_password = bcrypt.generate_password_hash("123").decode('utf-8')
    admin = User_table(username="Admin", firstname="Admin", lastname="Admin",email="Admin@local.nl",password=hashed_password)
    date = datetime(2000,1,1)
    testevent = Events_table(eventname="test", tag="@test", discription="test event",venue="test venue",eventstartingdate=date,eventendingdate=date)
    db.session.add(admin)
    db.session.add(testevent)
    db.session.commit()

try:
    test_input()
except:
    pass

