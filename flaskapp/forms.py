from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField, BooleanField, DateField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flaskapp.models import User_table


class RegistrationForm(FlaskForm):
    username = StringField("username", validators=[DataRequired(), Length(min=2, max=20)])
    firstname = StringField("firstname", validators=[DataRequired()])
    lastname = StringField("lastname", validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    confirm_password = PasswordField("Confirm password", validators=[DataRequired(), EqualTo("password")])
    submit = SubmitField("Sign up")

    def validate_username(self, username):
        user = User_table.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('That username is already in use!')

    def validate_email(self, email):
        user = User_table.query.filter_by(email=email.data).first()
        if user != None:
            raise ValidationError('That email is already in use!')

class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField("Password", validators=[DataRequired()])
    remember = BooleanField("Remember me")
    submit = SubmitField("Log in")

class EventsRegistrationForm(FlaskForm):
    eventname = StringField("eventname", validators=[DataRequired(), Length(min=2, max=20)])
    tag = StringField("tag", validators=[DataRequired()])
    discription= StringField("discription", validators=[DataRequired()])
    venue = StringField('venue', validators=[DataRequired()])
    eventstartingdate = DateField("eventstartingdate",validators=[DataRequired()])
    eventendingdate = DateField("eventendingdate", validators=[DataRequired()])
    submit = SubmitField("Create event")

