from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import *
from flask_bcrypt import Bcrypt
from flask_login import LoginManager





app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///app.db' #'pymysql://EventOptimizer:23wesd@localhost/Event_App'
app.config['SECRET_KEY'] = "Mega_Geheime_D1ingen"
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'
login_manager.login_message_category = 'info'

#######################################################################################
development_mode = True
#######################################################################################


from flaskapp import routes
